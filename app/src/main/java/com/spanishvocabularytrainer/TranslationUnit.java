package com.spanishvocabularytrainer;

public class TranslationUnit {
    private String mOrigin;
    private String mTranslation;

    public TranslationUnit(String mOrigin, String mTranslation) {
        this.mOrigin = mOrigin;
        this.mTranslation = mTranslation;
    }

    public String getmOrigin() {
        return mOrigin;
    }

    public void setmOrigin(String mOrigin) {
        this.mOrigin = mOrigin;
    }

    public String getmTranslation() {
        return mTranslation;
    }

    public void setmTranslation(String mTranslation) {
        this.mTranslation = mTranslation;
    }
}
