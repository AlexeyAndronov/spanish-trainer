package com.spanishvocabularytrainer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;

class WordListAdapter extends RecyclerView.Adapter<WordListAdapter.ViewHolder> {
    private ArrayList<TranslationUnit> mDataset;

    static class ViewHolder extends RecyclerView.ViewHolder {
        EditText mWordEditText;
        EditText mTranslationEditText;
        ViewHolder(View v) {
            super(v);
            this.mWordEditText = (EditText) v.findViewById(R.id.word_et);
            this.mTranslationEditText = (EditText) v.findViewById(R.id.translation_et);
        }
    }

    WordListAdapter(ArrayList<TranslationUnit> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public WordListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.word_list_elem, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(WordListAdapter.ViewHolder holder, int position) {
        holder.mWordEditText.setText(mDataset.get(position).getmOrigin());
        holder.mTranslationEditText.setText(mDataset.get(position).getmTranslation());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
